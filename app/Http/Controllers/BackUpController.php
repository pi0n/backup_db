<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class BackUpController extends Controller
{
    private function first_init($backup_file) 
    {
        $database = DB::getDatabaseName();
        Storage::put($backup_file, "CREATE DATABASE ".$database.";");
    }
    private function backup_table_config($table, $backup_file, $counter = 0) 
    {
        $table_link_config = DB::select('show create table '.$table);
        $table_link_config = (array)$table_link_config;
        $vault = (array)$table_link_config[0];
        Storage::append($backup_file, "DROP TABLE IF EXISTS '$table';");
        Storage::append($backup_file, $vault['Create Table']);
    }

    private function time_not_runout($time_start) 
    {
         $limit = ini_get('max_execution_time');
         $time_now = microtime(true);
         return (($limit - ($time_now - $time_start)) > 1);
    }

    public function backup(Request $request) 
    {
        //$request->session()->flush(); //require for debug of first iteration

        $time_start = microtime(true);

        if ($request->session()->has('backup_file')) {
            $backup_file = $request->session()->get('backup_file');
        } else {
            $mytime = date('Y-m-d_H:i:s');
            $backup_file = 'backup-'.$mytime.'.sql';
            $request->session()->put('backup_file', $backup_file);
            $this->first_init($backup_file);
        }

	$tables = DB::select('show tables');
        $tables = (array)$tables;
        $tables = array_map('current', $tables);
        $table_number = $request->session()->has('config') ? $request->session()->get('config') : 0;
        for(;$table_number < count($tables) && $this->time_not_runout($time_start); $table_number++) {
            $table = $tables[$table_number];
            DB::raw('lock tables ".$table." write');
            $this->backup_table_config($table, $backup_file);
        }
        if (!$this->time_not_runout($time_start)) {
            $request->session()->put('config', $table_number);
            return redirect()->action('BackUpController@backup');
        } else {
            $request->session()->put('config', count($tables)); //autostop for cycle at next round
        }

        $table_number = $request->session()->has('values_table') ? $request->session()->get('values_table') : 0;
        $record_number = $request->session()->has('values_record') ? $request->session()->get('values_record') : 0;

        for(; $table_number < count($tables) && $this->time_not_runout($time_start); $table_number++) {
            $table = $tables[$table_number];
            $size_table = DB::select('select count(*) from '.$table);
            $size_table = (array)$size_table[0];
            $offset_record = $request->session()->has('values_offset') ? $request->session()->get('values_offset') : 0;
            do {
                $remain_records = intval($size_table) - intval($offset_record);
                $current_records = DB::select('select * from '.$table.' limit '.$offset_record.','.$offset_record+=100);
                $current_records = (array)$current_records;
                $record_number = $request->session()->has('values_record') ? $request->session()->get('values_record') : 0;
                for(; $record_number < count($current_records) && $this->time_not_runout($time_start); $record_number++) {
                    $record = (array)$current_records[$record_number];
                    Storage::append($backup_file, "INSERT INTO ".$table." VALUES (");
                    $record = array_map(function($n) { return "'".addslashes($n)."'";}, $record);
                    Storage::append($backup_file, implode(",", $record));
                    Storage::append($backup_file, " ); ");
                }
            } while(($remain_records > 0) && ($this->time_not_runout($time_start)));
        }
        if (!$this->time_not_runout($time_start)) {
            $request->session()->put('values_table', $table_number);
            $request->session()->put('values_record', $record_number);
            $request->session()->put('values_offset', $offset_record);
            return redirect()->action('BackUpController@backup');
        }
        DB::raw('unlock tables');
        echo "Backup finished";
        $request->session()->forget('backup_file');
        $request->session()->forget('values_table');
        $request->session()->forget('values_record');
        $request->session()->forget('values_offset');
        return;
   }
}
