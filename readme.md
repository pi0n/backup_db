## Исполнение
Процедура бэкапа происходит при вызове маршрута '/backup'. Результат сопровождается надписью на странице. Вся логика реализована в файле app\Http\Controllers\BackUpController.php

## Лицензия

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
